--################################################################################
-- FILENAME   : Problem_3.sql
-- AUTHOR     : Brian Denton <brian.denton@gmail.com>
-- DATE       : 05/11/2013
-- PROJECT    : Coursera -- Introduction To Data Science -- Assignment 2
-- DESCRIPTION: Databases
--###############################################################################

-- Format output
.mode column
.headers on
--.tables

---------------------------------------------------------------------------------
-- Part (h)
---------------------------------------------------------------------------------

-- Compute D * transpose(D) where D is the raw frequency of terms in the Reuters dataset.

--.output ../output/similarity_matrix.txt

DROP TABLE TermCounts;
DROP TABLE N;
DROP TABLE TermFrequencies;
DROP TABLE D;
DROP TABLE S;

CREATE TABLE TermCounts AS SELECT term, SUM(count) AS TermCount FROM Frequency
  GROUP BY term;

CREATE TABLE N AS SELECT SUM(count) AS N FROM Frequency;

CREATE TABLE TermFrequencies AS SELECT *
  FROM TermCountsTemp, N;

CREATE TABLE D AS SELECT *, TermCount/(1.0*N) AS frequency FROM TermFrequencies;

-- Merge D with Frequency to convert term counts in each document to term
-- frequencies (also including terms with zero frequency in a particular
-- document). "Tranpose" this vector so columns represent terms and rows
-- represent documents - this might acutally be a GROUP BY operation.
-- This represents the D matrix.


CREATE TABLE S AS term, SELECT a.frequency * b.frequency AS d
  FROM D AS a, D as b WHERE a.term < b.term
  GROUP BY a.term;
   
SELECT * FROM S
  WHERE term LIKE "%world%";

.output stdout

--END OF FILE
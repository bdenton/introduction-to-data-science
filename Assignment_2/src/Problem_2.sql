--################################################################################
-- FILENAME   : Problem_2.sql
-- AUTHOR     : Brian Denton <brian.denton@gmail.com>
-- DATE       : 05/11/2013
-- PROJECT    : Coursera -- Introduction To Data Science -- Assignment 2
-- DESCRIPTION: Databases
--###############################################################################

-- Format output
--.mode column
--.headers on
--.tables

--SELECT * FROM a;
--SELECT * FROM b;

---------------------------------------------------------------------------------
-- Part (g)
---------------------------------------------------------------------------------

-- Implement matrix algebra of matrices a and b.

DROP TABLE c;

CREATE TABLE c AS SELECT a.row_num AS row_num, b.col_num AS col_num, SUM( a.value * b.value ) AS value
   FROM a, b WHERE a.col_num = b.row_num
   GROUP BY a.row_num, b.col_num;


.output ../output/multiply.txt

SELECT value FROM c
  WHERE row_num= 2 AND col_num = 3;

.output stdout

--END OF FILE
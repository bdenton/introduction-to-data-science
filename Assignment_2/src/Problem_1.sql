--################################################################################
-- FILENAME   : Problem_1.sql
-- AUTHOR     : Brian Denton <brian.denton@gmail.com>
-- DATE       : 05/11/2013
-- PROJECT    : Coursera -- Introduction To Data Science -- Assignment 2
-- DESCRIPTION: Databases
--###############################################################################

-- List all available databases
--.databases

-- Format output
--.mode column
--.headers on

-- See list of tables
--.tables

-- See schema of tables
--.schema Frequency

-- Get total count of records in table Frequency
--SELECT COUNT(*) FROM Frequency;

.output stdout 
---------------------------------------------------------------------------------
-- Part (a)
---------------------------------------------------------------------------------

-- Write output to text file
-- .output generates an error when the filename is preceded with the ../ in the
-- file path, but it still seems to properly direct output to the file. So I 
-- ignore the error.

.output ../output/select.txt

SELECT COUNT(*) FROM Frequency
  WHERE docid = "10398_txt_earn";

.output stdout                      -- Resume writing output to console

---------------------------------------------------------------------------------
-- Part (b)
---------------------------------------------------------------------------------

--πterm( σdocid=10398_txt_earn&count=1(frequency))

.output ../output/select_project.txt

SELECT COUNT(*) FROM Frequency
  WHERE docid = "10398_txt_earn" AND count = 1;

.output stdout

---------------------------------------------------------------------------------
-- Part (c)
---------------------------------------------------------------------------------

--πterm( σdocid=10398_txt_earn&count=1(frequency)) U πterm( σdocid=925_txt_trade&count=1(frequency))

.output ../output/union.txt

SELECT COUNT(*) FROM
( SELECT term FROM Frequency WHERE count = 1 AND docid IN( "10398_txt_earn" )
  UNION
  SELECT term FROM Frequency WHERE count = 1 AND docid IN( "925_txt_trade" ) );

.output stdout

---------------------------------------------------------------------------------
-- Part (d)
---------------------------------------------------------------------------------

--Write a SQL statement to count the number of documents containing the word “parliament”

.output ../output/count.txt

SELECT COUNT(*) FROM Frequency
  WHERE term = "parliament";

.output stdout

---------------------------------------------------------------------------------
-- Part (e)
---------------------------------------------------------------------------------

-- Get number of documents tht have sum of term counts > 300.

.output ../output/big_documents.txt

SELECT COUNT(*) FROM
  ( SELECT COUNT(*) FROM Frequency
  GROUP BY docid HAVING SUM(count) > 300 );


.output stdout

---------------------------------------------------------------------------------
-- Part (f)
---------------------------------------------------------------------------------

-- Get number of documents containg the words transactions and world.

.output ../output/two_words.txt

SELECT COUNT(*) FROM Frequency 
  WHERE term = "world" AND docid IN 
 (SELECT docid FROM Frequency 
    WHERE term  = "transactions" );

.output stdout

--END OF FILE
register s3n://uw-cse-344-oregon.aws.amazon.com/myudfs.jar

-- load the test file into Pig
raw = LOAD 's3n://uw-cse-344-oregon.aws.amazon.com/cse344-test-file' USING TextLoader as (line:chararray);
-- later you will load to other files, example:
--raw = LOAD 's3n://uw-cse-344-oregon.aws.amazon.com/btc-2010-chunk-000' USING TextLoader as (line:chararray); 


-- parse each line into ntriples
ntriples = foreach raw generate FLATTEN(myudfs.RDFSplit3(line)) as (subject:chararray,predicate:chararray,object:chararray);


-- filter ntriples to retain only records with subject matches '.*business.*'
business_ntriples = FILTER ntriples BY subject MATCHES '.*business.*';

-- make a copy of business_ntriples
business_ntriples2 = FOREACH business_ntriples GENERATE * AS (subject2:chararray, predicate2:chararray, object2:chararray);


-- join datasets
business = JOIN business_ntriples BY subject, business_ntriples2 BY subject2;

business = FOREACH business GENERATE * AS 
( subject:chararray, predicate:chararray, object:chararray, subject2:chararray, predicate2:chararray, object2:chararray );


-- filter for subject == subject2
business_filtered = FILTER business BY subject == subject2;

business_distinct = DISTINCT business_filtered;

-- store the results in the folder /user/hadoop/example-results
-- store count_by_subject_ordered into '/user/hadoop/example-results' using PigStorage();
-- Alternatively, you can store the results in S3, see instructions:
-- store count_by_subject_ordered into 's3n://superman/example-results';

STORE business_distinct INTO 's3n://introduction-to-data-science/problem-3A/' using PigStorage(',');
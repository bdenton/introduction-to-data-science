register s3n://uw-cse-344-oregon.aws.amazon.com/myudfs.jar

-- load the test file into Pig
-- raw = LOAD 's3n://uw-cse-344-oregon.aws.amazon.com/cse344-test-file' USING TextLoader as (line:chararray);
-- later you will load to other files, example:
-- raw = LOAD 's3n://uw-cse-344-oregon.aws.amazon.com/btc-2010-chunk-000' USING TextLoader as (line:chararray); 
raw = LOAD 's3n://uw-cse-344-oregon.aws.amazon.com/btc-2010-chunk-*' USING TextLoader as (line:chararray); 


-- parse each line into ntriples
ntriples = foreach raw generate FLATTEN(myudfs.RDFSplit3(line)) as (subject:chararray,predicate:chararray,object:chararray);

--group the n-triples by object column
--objects = group ntriples by (object) PARALLEL 50;

--group the n-triples by subject column
subjects = GROUP ntriples BY (subject) PARALLEL 50;

-- flatten the objects out (because group by produces a tuple of each object
-- in the first column, and we want each object ot be a string, not a tuple),
-- and count the number of tuples associated with each object
count_by_subject = FOREACH subjects GENERATE FLATTEN($0), COUNT($1) AS number_of_subjects PARALLEL 50;

--group the subjects by count
count_by_subject_count = GROUP count_by_subject BY (number_of_subjects) PARALLEL 50;



-- flatten the objects out (because group by produces a tuple of each object
-- in the first column, and we want each object ot be a string, not a tuple),
-- and count the number of tuples associated with each object
histogram_count = FOREACH count_by_subject_count GENERATE FLATTEN($0) AS number_of_subjects, COUNT($1) AS count PARALLEL 50;



--order the resulting tuples by their count in descending order
histogram_count_ordered = ORDER histogram_count BY (count)  PARALLEL 50;


-- store the results in the folder /user/hadoop/example-results
-- store count_by_subject_ordered into '/user/hadoop/example-results' using PigStorage();
-- Alternatively, you can store the results in S3, see instructions:
-- store count_by_subject_ordered into 's3n://superman/example-results';
STORE histogram_count_ordered INTO 's3n://introduction-to-data-science/problem-4/' using PigStorage();
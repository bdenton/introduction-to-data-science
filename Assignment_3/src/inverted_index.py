#!/usr/bin/env python
#################################################################################
# FILENAME   : inverted_index.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 05/18/2013
# PROJECT    : Coursera -- Introduction To Data Science -- Assignment 3
# DESCRIPTION: MapReduce
#################################################################################

import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[0]
    value = record[1]
    words = value.split()
    for w in words:
      mr.emit_intermediate(w, key)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrences
    source = set()
    for v in list_of_values:
      source.add( v )
    mr.emit((key, list(source)))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)


## END OF FILE

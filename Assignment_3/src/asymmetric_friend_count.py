#!/usr/bin/env python
#################################################################################
# FILENAME   : asymmetric_friend_count.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 05/18/2013
# PROJECT    : Coursera -- Introduction To Data Science -- Assignment 3
# DESCRIPTION: MapReduce
#################################################################################

import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[0]
    value = record[1]
    mr.emit_intermediate(key,value)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrences

    for v in list_of_values:
        if mr.intermediate.get( v ) is None:
            mr.emit(( ( key, v ) ))
            mr.emit(( ( v, key ) ))
        else:
            if key not in mr.intermediate.get( v ):
                mr.emit(( ( key, v ) ))
                mr.emit(( ( v, key ) ))
            
# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)


## END OF FILE

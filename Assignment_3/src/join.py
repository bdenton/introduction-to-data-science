#!/usr/bin/env python
#################################################################################
# FILENAME   : join.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 05/18/2013
# PROJECT    : Coursera -- Introduction To Data Science -- Assignment 3
# DESCRIPTION: MapReduce
#################################################################################

import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    table = record[0]
    order_id = record[1]
    values = record
    mr.emit_intermediate( order_id, (table, values) )

def reducer(key, list_of_values):

    for v in list_of_values:
        if v[0] == "order":
            order = v[1]
            
    for v in list_of_values:
        if v[0] != "order":
            mr.emit(( order + v[1] ))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)


## END OF FILE

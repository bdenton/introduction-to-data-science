#!/usr/bin/env python
#################################################################################
# FILENAME   : multiply.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 05/18/2013
# PROJECT    : Coursera -- Introduction To Data Science -- Assignment 3
# DESCRIPTION: MapReduce
#################################################################################

import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

A_NROWS = 5
A_NCOLS = 5

B_NROWS = 5
B_NCOLS = 5

def mapper(record):
    # key: document identifier
    # value: document contents
    matrix = record[0]
    row = record[1]
    col = record[2]
    val = record[3]
    if matrix == "a":
        for aj in range( A_NCOLS ):
            mr.emit_intermediate( (row,aj), (matrix,col,val) )
    else:
        for bi in range( B_NCOLS ):
            mr.emit_intermediate( (bi,col), (matrix,row,val) )

def reducer( key, list_of_values ):
    # key: word
    # value: list of occurrences

    dot_product = 0

    dot_product_dict = {}

    for v in list_of_values:
        matrix = v[0]
        index = v[1]
        value = v[2]

        if dot_product_dict.get( index ) is not None:
            dot_product_dict[ index ] = dot_product_dict.get( index ) + [ value ]
        else:
            dot_product_dict[ index ] = [ value ]
            
    for key2, val2 in dot_product_dict.items():
        if len( val2 ) == 2:
            dot_product += val2[0] * val2[1]
                
    mr.emit( (key[0], key[1], dot_product) )
            
# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)


## END OF FILE

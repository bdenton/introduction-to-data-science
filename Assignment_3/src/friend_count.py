#!/usr/bin/env python
#################################################################################
# FILENAME   : friend_count.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 05/18/2013
# PROJECT    : Coursera -- Introduction To Data Science -- Assignment 3
# DESCRIPTION: MapReduce
#################################################################################

import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[0]
    value = 1
    mr.emit_intermediate(key,value)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrences
    friend_count = 0

    for v in list_of_values:
      friend_count += v

    mr.emit((key, friend_count ))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)


## END OF FILE

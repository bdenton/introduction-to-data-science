import sys
import json
import string
import codecs
import re

def hw():
    print 'Hello, world!'

def lines(fp):
    print str(len(fp.readlines()))


def get_sentiment_dictionary( sent_file ):

    sentiment = {}

    for line in sent_file:
        line = line.split("\t")
        word = line[0].strip('\n')
        sent_value =  float( line[1] )
        sentiment[ word ] = sent_value

    return sentiment


def word_sentiment( word, sent_dict ):

    word_sent = 0

    if sent_dict.get( word ) is not None:
        word_sent = sent_dict.get( word )

    return word_sent


def tweet_sentiment( tweet, sent_dict ):

    tweet_sent = float(0)

    tweet = tweet.split() 

    for word in tweet:
        tweet_sent = tweet_sent + word_sentiment( word, sent_dict )

    return tweet_sent


def term_sentiment( tweet, sent_dict, terms_dict ):

    tweet_sent = float(0)

    tweet_words = set( tweet.split() )

    for word in tweet_words:

        if sent.dict.get( word ) is None:

            if terms.dict.get( word ) is None:
                terms.dict[ word ] = tweet_sentiment( tweet, sent_dict )
            else:
                terms.dict[ word ] = terms.dict[ word ] + tweet_sentiment( tweet, sent_dict )

    return terms_dict


def get_word_set( tweet_file ):
    
    WORDS = []
    
    for line in tweet_file:
        tweet = json.loads( line )
        if tweet.get( "text" ) is not None:
            tweet = tweet["text"]

            WORDS.extend( tweet.split() )

    return set( WORDS )


def main():
    sent_file = codecs.open( sys.argv[1], encoding = 'utf-8' )
    tweet_file = codecs.open( sys.argv[2], encoding = 'utf-8' )
    
    sent_dict = get_sentiment_dictionary( sent_file )
    
    # 1. Make a dictionary of words: word->sentiment
    #    If the word appears in AFINN-111.txt use that sentiment value
    #       Otherwise, initialize the sentiment value to zero
    
    WORD_SET = get_word_set( tweet_file )
    
    tweet_file.seek(0)
    
    new_word_sent = {}
            
            
# 2. Make a dictionary of tweets:  tweet->sentiment
    tweet_dict = {}
            
    for line in tweet_file:
        tweet = json.loads( line )
        if tweet.get( "text" ) is not None:
            tweet = tweet["text"]

            tweet_sent = tweet_sentiment( tweet, sent_dict )
            
            for word in set( tweet.split() ):
                
                if sent_dict.get( word ) is None:

                    if new_word_sent.get( word ) is not None:
                        new_word_sent[ word ] = new_word_sent[ word ] + tweet_sent
                    else:
                        new_word_sent[ word ] = tweet_sent

    for word, word_sent in new_word_sent.iteritems():
        word_split = word.split()
        for w in word_split:
            try:
                print str( word ), word_sent
            except:
                continue

if __name__ == '__main__':
    main()

## END OF FILE

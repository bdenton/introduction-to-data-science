#!/usr/bin/env python
#################################################################################
# FILENAME   : print.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 05/05/2013
# PROJECT    : Coursera -- Introduction To Data Science -- Problem 0
# DESCRIPTION: Twitter feed
#################################################################################

import urllib
import json

SEARCH_TERM = "Microsoft"
NUM_PAGES = 10

for PAGE in range(1,NUM_PAGES+1):

    URL = "http://search.twitter.com/search.json?q=" + SEARCH_TERM + "&page=" + str(PAGE)
    response = json.load( urllib.urlopen( URL ) )

    for TWEET in response["results"]:
        print TWEET["text"]


## END OF FILE


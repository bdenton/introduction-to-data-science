import sys
import json
import string
import codecs

def hw():
    print 'Hello, world!'

def lines(fp):
    print str(len(fp.readlines()))

def get_sentiment_dictionary( sent_file ):

    sentiment = {}

    for line in sent_file:
        line = line.split("\t")
        word = line[0]
        sent_value =  float( line[1] )
        sentiment[ word ] = sent_value

    return sentiment


def word_sentiment( word, sent_dict ):

    word_sent = 0

    if sent_dict.get( word ) is not None:
        word_sent = sent_dict.get( word )

    return word_sent


def tweet_sentiment( tweet, sent_dict ):

    tweet_sent = 0

    tweet = tweet.split()

    for word in tweet:
        tweet_sent = tweet_sent + word_sentiment( word, sent_dict )

    return tweet_sent


def main():
    sent_file = codecs.open( sys.argv[1], encoding = 'utf-8' )
    tweet_file = codecs.open( sys.argv[2], encoding = 'utf-8' )

    sent_dict = get_sentiment_dictionary( sent_file )

    for line in tweet_file:
        tweet = json.loads( line )
        if tweet.get( "text" ) is not None:
            tweet = tweet["text"]
            tweet = filter( lambda x: x not in string.punctuation, tweet )
            tweet = tweet.lower()
            print tweet_sentiment( tweet, sent_dict )

if __name__ == '__main__':
    main()

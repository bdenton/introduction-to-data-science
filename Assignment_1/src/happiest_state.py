#!/usr/bin/env python
#################################################################################
# FILENAME   : happiest_state.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 05/05/2013
# PROJECT    : Coursera -- Introduction To Data Science -- Problem 0
# DESCRIPTION: Twitter feed
#################################################################################

import sys
import json
import string

sent_file = open( sys.argv[1] )
#sent_file = open( "AFINN-111.txt" )

tweet_file = open( sys.argv[2] )
#tweet_file = open( "../output/output.txt" )

STATES1 = ['AL','AK','AZ','AR','CA','CO','CT','DE','DC','FL','GA','HI','ID']
STATES2 = ['IL','IN','IA','KS','KY','LA','ME','MT','NE','NV','NH','NJ','NM']
STATES3 = ['NY','NC','ND','OH','OK','OR','MD','MA','MI','MN','MS','MO','PA'] 
STATES4 = ['RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY']

US_STATES = STATES1 + STATES2 + STATES3 + STATES4

def get_sentiment_dictionary( sent_file ):

    sentiment = {}

    for line in sent_file:
        line = line.split("\t")
        word = line[0]
        sent_value =  float( line[1] )
        sentiment[ word ] = sent_value

    return sentiment


def word_sentiment( word, sent_dict ):

    word_sent = 0

    if sent_dict.get( word ) is not None:
        word_sent = sent_dict.get( word )

    return word_sent


def tweet_sentiment( tweet, sent_dict ):

    tweet_sent = 0

    tweet = tweet.split()

    for word in tweet:
        tweet_sent = tweet_sent + word_sentiment( word, sent_dict )

    return tweet_sent



sent_dict = get_sentiment_dictionary( sent_file )

state_sentiment = {}


for line in tweet_file:
    tweet = json.loads( line )
    if tweet.get( "text" ) is not None and tweet.get("place") is not None:
        text = tweet["text"]
        text = text.lower()
        place = tweet.get("place").get("full_name")
        state = place.split(",")[-1].strip().upper()
        if state in US_STATES:
            sentiment = tweet_sentiment( text, sent_dict )
        
            if state_sentiment.get( state ) is not None:
                sentiment = state_sentiment[ state ][0] + sentiment
                tweet_count = state_sentiment[ state ][1] + 1
                state_sentiment[ state ] = ( sentiment, tweet_count )
            else:
                state_sentiment[ state ] = ( sentiment, 1 )


# Compute average sentiment per number of tweets
# Rank and print happeiest state

state_happiness = {}

for state, ( sentiment, tweet_count ) in state_sentiment.iteritems():
    if tweet_count > 0:
        state_happiness[ state ] = float( sentiment )/tweet_count


inverse = [(value, key) for key, value in state_happiness.items()]

print max(inverse)[1]

## END OF FILE

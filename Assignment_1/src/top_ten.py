#!/usr/bin/env python
#################################################################################
# FILENAME   : top_ten.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 05/05/2013
# PROJECT    : Coursera -- Introduction To Data Science -- Problem 0
# DESCRIPTION: Twitter feed
#################################################################################

import sys
import json
import string
import re
import codecs

tweet_file = codecs.open( sys.argv[1], encoding = 'utf-8' )
#tweet_file = codecs.open( "../output/output.txt", encoding = 'utf-8' )

hash_tags = {}

for line in tweet_file:

    tweet = json.loads( line )

    if tweet.get("entities") is not None:

        if tweet.get("entities").get("hashtags") is not None:

            hashtags = tweet.get("entities").get("hashtags")
                
            for tag in hashtags:

                exctracted_tag = repr( tag.get("text") )

                if hash_tags.get( exctracted_tag ) is not None:
                    hash_tags[ exctracted_tag ] = hash_tags[ exctracted_tag ] + 1

                else:
                    hash_tags[ exctracted_tag ] = 1
                        


inverse = [(value, key) for key, value in hash_tags.items()]

top_hash_tags = sorted( inverse, key = lambda inverse: inverse[0], reverse = True )

top_hash_tags = top_hash_tags[0:10]

for count, tag in top_hash_tags:
    print tag, float(count)


## END OF FILE

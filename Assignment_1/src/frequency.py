#!/usr/bin/env python
#################################################################################
# FILENAME   : print.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 05/05/2013
# PROJECT    : Coursera -- Introduction To Data Science -- Problem 0
# DESCRIPTION: Twitter feed
#################################################################################

import sys
import json
import string

tweet_file = open( sys.argv[1] )
#tweet_file = open( "../output/output.txt" )

tweet_word_list = list()


for line in tweet_file:
    tweet = json.loads( line )
    if tweet.get( "text" ) is not None:
        tweet = tweet["text"]
        #tweet = filter( lambda x: x not in string.punctuation, tweet ) # don't remove emoticons
        tweet = tweet.lower()
        tweet_word_list.extend( tweet.split() )
        

tweet_word_list.sort()

#print "Constructed word list..."

tweet_word_set = set( tweet_word_list )

#print "Constructed word set..."


N = float( len( tweet_word_list ) )

for word in tweet_word_set:
    try:
        print word, tweet_word_list.count( word )/N
    except:
        continue

## END OF FILE
